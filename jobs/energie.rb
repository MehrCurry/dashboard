require 'json'
require 'pp'

# :first_in sets how long it takes before the job is first run. In this case, it is run immediately
SCHEDULER.every '5s', :first_in => 0 do |job|
  conn = Faraday.new

  response = conn.get 'http://cubieez:8081/api/get/Tages%20Leistung'
  json=JSON.parse(response.body)
  pp json
  send_event('solar-ertrag', { value: json["value"] })
end

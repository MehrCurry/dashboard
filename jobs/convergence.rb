require 'json'

# Populate the graph with some random points
points = []
(1..10).each do |i|
  points << { x: i, y: rand(50) }
end
last_x = points.last[:x]

SCHEDULER.every '30s' do
  conn = Faraday.new
  response = conn.get 'http://cubieez:8081/api/get/Momentan%20Leistung'
  json=JSON.parse(response.body)
  print json["value"] + "\n"

  points.shift
  last_x += 1
  points << { x: last_x, y: json["value"] }

  send_event('convergence', points: points)
end

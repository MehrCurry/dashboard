require 'json'
require 'pp'
require 'faraday'

# :first_in sets how long it takes before the job is first run. In this case, it is run immediately
SCHEDULER.every '60s', :first_in => 0 do |job|
  conn = Faraday.new
  response = conn.get 'http://cubieez:8081/api/get/yr.no%20Wettervorhersage'
  json=JSON.parse(response.body)
  print json["value"] + "\n"
  send_event('welcome', { moreinfo: json["value"] })
end

require 'json'
require 'pp'

# :first_in sets how long it takes before the job is first run. In this case, it is run immediately
SCHEDULER.every '5s', :first_in => 0 do |job|
  conn = Faraday.new
  response = conn.get 'http://cubieez:8081/api/get/Momentan%20Leistung'
  json=JSON.parse(response.body)
  print json["value"] + "\n"
  send_event('solar', { value: json["value"] })
end
